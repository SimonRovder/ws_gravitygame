package decoder;

import java.util.LinkedList;
import java.util.regex.Pattern;

public class Decoder {
	
	public LinkedList<XYC> nodes;
	public LinkedList<XYC> circles;
	public LinkedList<XYXYC> connections;
	public char type;
	public LinkedList<String> messages;
	public LinkedList<String> errors;
	
	public static void main(String args[]){
		Decoder d = new Decoder();
		d.decode("[[]]S[[]]Welcome to the game.[###]No errors [###]2181[&amp;&amp;&amp;]962[&amp;&amp;&amp;]0[;;;]1909[&amp;&amp;&amp;]1144[&amp;&amp;&amp;]0[;;;]2012[&amp;&amp;&amp;]864[&amp;&amp;&amp;]0[;;;]1570[&amp;&amp;&amp;]1035[&amp;&amp;&amp;]0[;;;]1962[&amp;&amp;&amp;]702[&amp;&amp;&amp;]0[;;;]1931[&amp;&amp;&amp;]1024[&amp;&amp;&amp;]0[;;;]1799[&amp;&amp;&amp;]814[&amp;&amp;&amp;]0[;;;]1860[&amp;&amp;&amp;]1088[&amp;&amp;&amp;]0[;;;]1987[&amp;&amp;&amp;]1085[&amp;&amp;&amp;]0[;;;]1653[&amp;&amp;&amp;]857[&amp;&amp;&amp;]0[;;;]1968[&amp;&amp;&amp;]1180[&amp;&amp;&amp;]0[;;;]2085[&amp;&amp;&amp;]987[&amp;&amp;&amp;]0[;;;]2137[&amp;&amp;&amp;]1056[&amp;&amp;&amp;]0[;;;]1891[&amp;&amp;&amp;]691[&amp;&amp;&amp;]0[;;;]2083[&amp;&amp;&amp;]817[&amp;&amp;&amp;]0[;;;]1889[&amp;&amp;&amp;]922[&amp;&amp;&amp;]0[;;;]1848[&amp;&amp;&amp;]958[&amp;&amp;&amp;]0[;;;]1959[&amp;&amp;&amp;]908[&amp;&amp;&amp;]0[;;;]2010[&amp;&amp;&amp;]987[&amp;&amp;&amp;]0[;;;]2223[&amp;&amp;&amp;]1001[&amp;&amp;&amp;]0[;;;]1554[&amp;&amp;&amp;]1146[&amp;&amp;&amp;]0[;;;]1649[&amp;&amp;&amp;]1319[&amp;&amp;&amp;]2[;;;]2046[&amp;&amp;&amp;]1195[&amp;&amp;&amp;]2[;;;]1721[&amp;&amp;&amp;]950[&amp;&amp;&amp;]2[;;;]1778[&amp;&amp;&amp;]963[&amp;&amp;&amp;]2[;;;]1658[&amp;&amp;&amp;]1019[&amp;&amp;&amp;]2[;;;]1779[&amp;&amp;&amp;]1107[&amp;&amp;&amp;]2[;;;]1631[&amp;&amp;&amp;]1170[&amp;&amp;&amp;]2[;;;]1754[&amp;&amp;&amp;]889[&amp;&amp;&amp;]2[;;;]1540[&amp;&amp;&amp;]913[&amp;&amp;&amp;]2[;;;]1501[&amp;&amp;&amp;]1058[&amp;&amp;&amp;]2[;;;]2101[&amp;&amp;&amp;]1141[&amp;&amp;&amp;]1[###]2046[&amp;&amp;&amp;]1195[&amp;&amp;&amp;]2[;;;]1631[&amp;&amp;&amp;]1170[&amp;&amp;&amp;]2[;;;]1754[&amp;&amp;&amp;]889[&amp;&amp;&amp;]2[;;;]1501[&amp;&amp;&amp;]1058[&amp;&amp;&amp;]2[;;;]2101[&amp;&amp;&amp;]1141[&amp;&amp;&amp;]1[###]1848[&amp;&amp;&amp;]958[&amp;&amp;&amp;]1889[&amp;&amp;&amp;]922[&amp;&amp;&amp;]0[;;;]2223[&amp;&amp;&amp;]1001[&amp;&amp;&amp;]2181[&amp;&amp;&amp;]962[&amp;&amp;&amp;]0[;;;]1778[&amp;&amp;&amp;]963[&amp;&amp;&amp;]1721[&amp;&amp;&amp;]950[&amp;&amp;&amp;]0[;;;]1959[&amp;&amp;&amp;]908[&amp;&amp;&amp;]2012[&amp;&amp;&amp;]864[&amp;&amp;&amp;]0[;;;]1968[&amp;&amp;&amp;]1180[&amp;&amp;&amp;]1909[&amp;&amp;&amp;]1144[&amp;&amp;&amp;]0[;;;]1754[&amp;&amp;&amp;]889[&amp;&amp;&amp;]1721[&amp;&amp;&amp;]950[&amp;&amp;&amp;]0[;;;]1848[&amp;&amp;&amp;]958[&amp;&amp;&amp;]1778[&amp;&amp;&amp;]963[&amp;&amp;&amp;]0[;;;]1959[&amp;&amp;&amp;]908[&amp;&amp;&amp;]1889[&amp;&amp;&amp;]922[&amp;&amp;&amp;]0[;;;]1891[&amp;&amp;&amp;]691[&amp;&amp;&amp;]1962[&amp;&amp;&amp;]702[&amp;&amp;&amp;]0[;;;]1570[&amp;&amp;&amp;]1035[&amp;&amp;&amp;]1501[&amp;&amp;&amp;]1058[&amp;&amp;&amp;]0[;;;]1860[&amp;&amp;&amp;]1088[&amp;&amp;&amp;]1909[&amp;&amp;&amp;]1144[&amp;&amp;&amp;]0[;;;]2085[&amp;&amp;&amp;]987[&amp;&amp;&amp;]2010[&amp;&amp;&amp;]987[&amp;&amp;&amp;]0[;;;]2101[&amp;&amp;&amp;]1141[&amp;&amp;&amp;]2046[&amp;&amp;&amp;]1195[&amp;&amp;&amp;]0[;;;]1778[&amp;&amp;&amp;]963[&amp;&amp;&amp;]1754[&amp;&amp;&amp;]889[&amp;&amp;&amp;]0[;;;]2046[&amp;&amp;&amp;]1195[&amp;&amp;&amp;]1968[&amp;&amp;&amp;]1180[&amp;&amp;&amp;]0[;;;]1554[&amp;&amp;&amp;]1146[&amp;&amp;&amp;]1631[&amp;&amp;&amp;]1170[&amp;&amp;&amp;]0[;;;]1931[&amp;&amp;&amp;]1024[&amp;&amp;&amp;]1987[&amp;&amp;&amp;]1085[&amp;&amp;&amp;]0[;;;]1860[&amp;&amp;&amp;]1088[&amp;&amp;&amp;]1779[&amp;&amp;&amp;]1107[&amp;&amp;&amp;]0[;;;]2083[&amp;&amp;&amp;]817[&amp;&amp;&amp;]2012[&amp;&amp;&amp;]864[&amp;&amp;&amp;]0[;;;]2137[&amp;&amp;&amp;]1056[&amp;&amp;&amp;]2085[&amp;&amp;&amp;]987[&amp;&amp;&amp;]0[;;;]2010[&amp;&amp;&amp;]987[&amp;&amp;&amp;]1931[&amp;&amp;&amp;]1024[&amp;&amp;&amp;]0[;;;]1754[&amp;&amp;&amp;]889[&amp;&amp;&amp;]1799[&amp;&amp;&amp;]814[&amp;&amp;&amp;]0[;;;]1658[&amp;&amp;&amp;]1019[&amp;&amp;&amp;]1570[&amp;&amp;&amp;]1035[&amp;&amp;&amp;]0[;;;]2137[&amp;&amp;&amp;]1056[&amp;&amp;&amp;]2101[&amp;&amp;&amp;]1141[&amp;&amp;&amp;]0[;;;]1658[&amp;&amp;&amp;]1019[&amp;&amp;&amp;]1721[&amp;&amp;&amp;]950[&amp;&amp;&amp;]0[;;;]2010[&amp;&amp;&amp;]987[&amp;&amp;&amp;]1959[&amp;&amp;&amp;]908[&amp;&amp;&amp;]0[;;;]1860[&amp;&amp;&amp;]1088[&amp;&amp;&amp;]1931[&amp;&amp;&amp;]1024[&amp;&amp;&amp;]0[;;;]1987[&amp;&amp;&amp;]1085[&amp;&amp;&amp;]1968[&amp;&amp;&amp;]1180[&amp;&amp;&amp;]0[;;;]1987[&amp;&amp;&amp;]1085[&amp;&amp;&amp;]1909[&amp;&amp;&amp;]1144[&amp;&amp;&amp;]0[;;;]2085[&amp;&amp;&amp;]987[&amp;&amp;&amp;]2181[&amp;&amp;&amp;]962[&amp;&amp;&amp;]0[;;;]1987[&amp;&amp;&amp;]1085[&amp;&amp;&amp;]2010[&amp;&amp;&amp;]987[&amp;&amp;&amp;]0[;;;]2137[&amp;&amp;&amp;]1056[&amp;&amp;&amp;]2223[&amp;&amp;&amp;]1001[&amp;&amp;&amp;]0[;;;]1501[&amp;&amp;&amp;]1058[&amp;&amp;&amp;]1554[&amp;&amp;&amp;]1146[&amp;&amp;&amp;]0[;;;]2137[&amp;&amp;&amp;]1056[&amp;&amp;&amp;]2181[&amp;&amp;&amp;]962[&amp;&amp;&amp;]0[;;;]1754[&amp;&amp;&amp;]889[&amp;&amp;&amp;]1653[&amp;&amp;&amp;]857[&amp;&amp;&amp;]0[;;;]1931[&amp;&amp;&amp;]1024[&amp;&amp;&amp;]1848[&amp;&amp;&amp;]958[&amp;&amp;&amp;]0[;;;]1889[&amp;&amp;&amp;]922[&amp;&amp;&amp;]1931[&amp;&amp;&amp;]1024[&amp;&amp;&amp;]0[;;;]1554[&amp;&amp;&amp;]1146[&amp;&amp;&amp;]1570[&amp;&amp;&amp;]1035[&amp;&amp;&amp;]0[;;;]1653[&amp;&amp;&amp;]857[&amp;&amp;&amp;]1721[&amp;&amp;&amp;]950[&amp;&amp;&amp;]0[;;;]1754[&amp;&amp;&amp;]889[&amp;&amp;&amp;]1848[&amp;&amp;&amp;]958[&amp;&amp;&amp;]0[;;;]1959[&amp;&amp;&amp;]908[&amp;&amp;&amp;]1931[&amp;&amp;&amp;]1024[&amp;&amp;&amp;]0[;;;]1909[&amp;&amp;&amp;]1144[&amp;&amp;&amp;]1931[&amp;&amp;&amp;]1024[&amp;&amp;&amp;]0[;;;]2012[&amp;&amp;&amp;]864[&amp;&amp;&amp;]2010[&amp;&amp;&amp;]987[&amp;&amp;&amp;]0[;;;]1987[&amp;&amp;&amp;]1085[&amp;&amp;&amp;]2046[&amp;&amp;&amp;]1195[&amp;&amp;&amp;]0[;;;]1540[&amp;&amp;&amp;]913[&amp;&amp;&amp;]1570[&amp;&amp;&amp;]1035[&amp;&amp;&amp;]0[;;;]1540[&amp;&amp;&amp;]913[&amp;&amp;&amp;]1653[&amp;&amp;&amp;]857[&amp;&amp;&amp;]0[;;;]2101[&amp;&amp;&amp;]1141[&amp;&amp;&amp;]1987[&amp;&amp;&amp;]1085[&amp;&amp;&amp;]0[;;;]1860[&amp;&amp;&amp;]1088[&amp;&amp;&amp;]1848[&amp;&amp;&amp;]958[&amp;&amp;&amp;]0[;;;]1778[&amp;&amp;&amp;]963[&amp;&amp;&amp;]1658[&amp;&amp;&amp;]1019[&amp;&amp;&amp;]0[;;;]1779[&amp;&amp;&amp;]1107[&amp;&amp;&amp;]1909[&amp;&amp;&amp;]1144[&amp;&amp;&amp;]0[;;;]2012[&amp;&amp;&amp;]864[&amp;&amp;&amp;]1889[&amp;&amp;&amp;]922[&amp;&amp;&amp;]0[;;;]2085[&amp;&amp;&amp;]987[&amp;&amp;&amp;]1987[&amp;&amp;&amp;]1085[&amp;&amp;&amp;]0[;;;]1754[&amp;&amp;&amp;]889[&amp;&amp;&amp;]1889[&amp;&amp;&amp;]922[&amp;&amp;&amp;]0[;;;]1889[&amp;&amp;&amp;]922[&amp;&amp;&amp;]1799[&amp;&amp;&amp;]814[&amp;&amp;&amp;]0[;;;]2012[&amp;&amp;&amp;]864[&amp;&amp;&amp;]2085[&amp;&amp;&amp;]987[&amp;&amp;&amp;]0[;;;]1778[&amp;&amp;&amp;]963[&amp;&amp;&amp;]1779[&amp;&amp;&amp;]1107[&amp;&amp;&amp;]0[;;;]1570[&amp;&amp;&amp;]1035[&amp;&amp;&amp;]1631[&amp;&amp;&amp;]1170[&amp;&amp;&amp;]0[;;;]1860[&amp;&amp;&amp;]1088[&amp;&amp;&amp;]1778[&amp;&amp;&amp;]963[&amp;&amp;&amp;]0[;;;]1779[&amp;&amp;&amp;]1107[&amp;&amp;&amp;]1658[&amp;&amp;&amp;]1019[&amp;&amp;&amp;]0[;;;]1649[&amp;&amp;&amp;]1319[&amp;&amp;&amp;]1631[&amp;&amp;&amp;]1170[&amp;&amp;&amp;]0[;;;]1501[&amp;&amp;&amp;]1058[&amp;&amp;&amp;]1540[&amp;&amp;&amp;]913[&amp;&amp;&amp;]0[;;;]1799[&amp;&amp;&amp;]814[&amp;&amp;&amp;]1653[&amp;&amp;&amp;]857[&amp;&amp;&amp;]0[;;;]1987[&amp;&amp;&amp;]1085[&amp;&amp;&amp;]2137[&amp;&amp;&amp;]1056[&amp;&amp;&amp;]0[;;;]1631[&amp;&amp;&amp;]1170[&amp;&amp;&amp;]1658[&amp;&amp;&amp;]1019[&amp;&amp;&amp;]0[;;;]1799[&amp;&amp;&amp;]814[&amp;&amp;&amp;]1891[&amp;&amp;&amp;]691[&amp;&amp;&amp;]0[;;;]1540[&amp;&amp;&amp;]913[&amp;&amp;&amp;]1658[&amp;&amp;&amp;]1019[&amp;&amp;&amp;]0[;;;]1779[&amp;&amp;&amp;]1107[&amp;&amp;&amp;]1631[&amp;&amp;&amp;]1170[&amp;&amp;&amp;]0[;;;]1653[&amp;&amp;&amp;]857[&amp;&amp;&amp;]1658[&amp;&amp;&amp;]1019[&amp;&amp;&amp;]0[;;;]2083[&amp;&amp;&amp;]817[&amp;&amp;&amp;]1962[&amp;&amp;&amp;]702[&amp;&amp;&amp;]0[;;;]1962[&amp;&amp;&amp;]702[&amp;&amp;&amp;]2012[&amp;&amp;&amp;]864[&amp;&amp;&amp;]0[;;;]2085[&amp;&amp;&amp;]987[&amp;&amp;&amp;]2083[&amp;&amp;&amp;]817[&amp;&amp;&amp;]0[;;;]2083[&amp;&amp;&amp;]817[&amp;&amp;&amp;]2181[&amp;&amp;&amp;]962[&amp;&amp;&amp;]0[;;;]2223[&amp;&amp;&amp;]1001[&amp;&amp;&amp;]2101[&amp;&amp;&amp;]1141[&amp;&amp;&amp;]0[;;;]1649[&amp;&amp;&amp;]1319[&amp;&amp;&amp;]1554[&amp;&amp;&amp;]1146[&amp;&amp;&amp;]0[;;;]1962[&amp;&amp;&amp;]702[&amp;&amp;&amp;]1799[&amp;&amp;&amp;]814[&amp;&amp;&amp;]0[;;;]1779[&amp;&amp;&amp;]1107[&amp;&amp;&amp;]1968[&amp;&amp;&amp;]1180[&amp;&amp;&amp;]0[;;;]1799[&amp;&amp;&amp;]814[&amp;&amp;&amp;]2012[&amp;&amp;&amp;]864[&amp;&amp;&amp;]0[;;;]2083[&amp;&amp;&amp;]817[&amp;&amp;&amp;]2223[&amp;&amp;&amp;]1001[&amp;&amp;&amp;]0[;;;]1649[&amp;&amp;&amp;]1319[&amp;&amp;&amp;]1779[&amp;&amp;&amp;]1107[&amp;&amp;&amp;]0[;;;]1653[&amp;&amp;&amp;]857[&amp;&amp;&amp;]1891[&amp;&amp;&amp;]691[&amp;&amp;&amp;]0[;;;]1649[&amp;&amp;&amp;]1319[&amp;&amp;&amp;]1501[&amp;&amp;&amp;]1058[&amp;&amp;&amp;]0[;;;]1968[&amp;&amp;&amp;]1180[&amp;&amp;&amp;]1649[&amp;&amp;&amp;]1319[&amp;&amp;&amp;]0[;;;]1891[&amp;&amp;&amp;]691[&amp;&amp;&amp;]1540[&amp;&amp;&amp;]913[&amp;&amp;&amp;]0[;;;]2046[&amp;&amp;&amp;]1195[&amp;&amp;&amp;]1649[&amp;&amp;&amp;]1319[&amp;&amp;&amp;]0[;;;]2046[&amp;&amp;&amp;]1195[&amp;&amp;&amp;]1649[&amp;&amp;&amp;]1319[&amp;&amp;&amp;]2[;;;]1631[&amp;&amp;&amp;]1170[&amp;&amp;&amp;]1779[&amp;&amp;&amp;]1107[&amp;&amp;&amp;]2[;;;]1779[&amp;&amp;&amp;]1107[&amp;&amp;&amp;]1658[&amp;&amp;&amp;]1019[&amp;&amp;&amp;]2[;;;]1658[&amp;&amp;&amp;]1019[&amp;&amp;&amp;]1778[&amp;&amp;&amp;]963[&amp;&amp;&amp;]2[;;;]1778[&amp;&amp;&amp;]963[&amp;&amp;&amp;]1721[&amp;&amp;&amp;]950[&amp;&amp;&amp;]2[;;;]1501[&amp;&amp;&amp;]1058[&amp;&amp;&amp;]1540[&amp;&amp;&amp;]913[&amp;&amp;&amp;]2[[]]");
		d.printData();
	}
	
	public Decoder(){
		this.nodes = new LinkedList<XYC>();
		this.connections = new LinkedList<XYXYC>();
		this.circles = new LinkedList<XYC>();
		this.errors = new LinkedList<String>();
		this.messages = new LinkedList<String>();
	}
	
	public void printData(){
		t("Type: " + this.type);
		t("ERRORS: ");
		for(String s : errors){
			t(s);
		}
		t("MESSAGES: ");
		for(String s : messages){
			t(s);
		}
		t("NODES: ");
		for(XYC s : nodes){
			t(s.toString());
		}
		t("CIRCLES: ");
		for(XYC s : circles){
			t(s.toString());
		}
		t("CONNECTIONS: ");
		for(XYXYC s : connections){
			t(s.toString());
		}
	}
	
	private void t(String s){
		System.out.println(s);
	}
	
	public void decode(String s){
		s = s.replaceAll(Pattern.quote("&amp;"), "&");
		String base[] = s.split(Pattern.quote("[[]]"));
		this.type = base[1].toCharArray()[0];
		String sections[] = base[2].split(Pattern.quote("[###]"));
		String messagesA[] = sections[0].split(Pattern.quote("[;;;]"));
		for(String i : messagesA){
			this.messages.add(i);
		}
		String errorsA[] = sections[1].split(Pattern.quote("[;;;]"));
		for(String i : errorsA){
			this.errors.add(i);
		}
		String[] nodesA = sections[2].split(Pattern.quote("[;;;]"));
		for(String i : nodesA){
			this.nodes.add(XYC.toXYC(i));
		}
		String[] circlesA = sections[3].split(Pattern.quote("[;;;]"));
		for(String i : circlesA){
			this.circles.add(XYC.toXYC(i));
		}
		String[] connectionsA = sections[4].split(Pattern.quote("[;;;]"));
		for(String i : connectionsA){
			this.connections.add(XYXYC.toXYXYC(i));
		}
	}
}
