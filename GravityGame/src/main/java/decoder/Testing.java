package decoder;

import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;

public class Testing {
	public static void main(String[] args){
		JFileChooser chooser = new JFileChooser();
		chooser.setCurrentDirectory(new java.io.File("."));
		chooser.setDialogTitle("select folder");
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		chooser.setAcceptAllFileFilterUsed(false);
		chooser.showOpenDialog(new JButton());
		
		String link = chooser.getSelectedFile().getAbsolutePath();
		System.out.println(link);
		File folder = new File(link);
		System.out.println(folder.getAbsolutePath());
		File[] listOfFiles = folder.listFiles();
		for (File f : listOfFiles){
			System.out.println(f.getAbsolutePath());
		}
	}
}
