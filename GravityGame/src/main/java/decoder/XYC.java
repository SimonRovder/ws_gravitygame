package decoder;

import java.util.regex.Pattern;

public class XYC {
	public short x;
	public short y;
	public byte color;
	
	
	public XYC(short x, short y, byte color) {
		super();
		this.x = x;
		this.y = y;
		this.color = color;
	}
	
	public static XYC toXYC(String s){
		String a[] = s.split(Pattern.quote("[&&&]"));
		return new XYC(Short.parseShort(a[0]), Short.parseShort(a[1]), Byte.parseByte(a[2]));
	}
	
	public String toString(){
		return "X: " + this.x + " --- Y: " + this.y + " --- Color: " + this.color;
	}
}
