package decoder;

import java.util.regex.Pattern;

public class XYXYC {
	public short x1;
	public short y1;
	public short x2;
	public short y2;
	public byte color;
	
	public XYXYC(short x1, short y1, short x2, short y2, byte color) {
		super();
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
		this.color = color;
	}
	
	public static XYXYC toXYXYC(String s){
		String[] a = s.split(Pattern.quote("[&&&]"));
		return new XYXYC(Short.parseShort(a[0]), Short.parseShort(a[1]), Short.parseShort(a[2]), Short.parseShort(a[3]), Byte.parseByte(a[4]));
	}

	public String toString(){
		return "X1: " + this.x1 + " --- Y1: " + this.y1 + " --- X2: " + this.x2 + " --- Y2: " + this.y2 + " --- Color: " + this.color;
	}
	
}
