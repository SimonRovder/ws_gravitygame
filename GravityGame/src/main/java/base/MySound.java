package base;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;

public class MySound extends Sound{

	public String link;
	public String name;
	
	public MySound(String link, String name) throws SlickException {
		super(link);
		this.name = name;
		this.link = link;
	}

}
