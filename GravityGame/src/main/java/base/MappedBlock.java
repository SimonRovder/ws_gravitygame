package base;


public class MappedBlock {
	public BasicBlock block;
	public int x,y;
	public BlockAction action;
	public int referenceId;
	public MySound sound;
	public boolean played;
	
	public MappedBlock(BasicBlock block, int x, int y, int ref, MySound sound) {
		super();
		this.sound = sound;
		this.played = false;
		this.block = block;
		this.x = x;
		this.y = y;
		this.referenceId = ref;
	}

	public String toString(){
		if(this.sound != null){
			return x + ";;;" + y + ";;;" + referenceId + ";;;" + this.block.id + ";;;" + this.sound.name;
		}else{
			return x + ";;;" + y + ";;;" + referenceId + ";;;" + this.block.id + ";;;";
		}
	}

	public static MappedBlock parseMappedBlock(Resources r, String mapped, String act) {
		System.out.println(mapped);
		String arr[] = mapped.split(";;;");
		System.out.println(arr.length);
		int x = Integer.parseInt(arr[0]);
		int y = Integer.parseInt(arr[1]);
		int ref = Integer.parseInt(arr[2]);
		BasicBlock b = r.getBlockById(Integer.parseInt(arr[3]));
		MappedBlock mb;
		if(arr.length > 4){
			mb = new MappedBlock(b, x, y, ref, r.getSound(arr[4]));
		}else{
			mb = new MappedBlock(b, x, y, ref, null);
		}
		mb.setAction(BlockAction.parseAction(act));
		return mb;
	}

	private void setAction(BlockAction action) {
		this.action = action;
	}
	
	public void playSound(){
		if(this.sound != null){
			if(!this.played){
				this.played = true;
				this.sound.play();
			}
		}
	}
}
