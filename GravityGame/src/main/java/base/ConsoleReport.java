package base;

public class ConsoleReport {
	public String message;
	public int delay;
	public ConsoleReport(int delay, String message){
		this.message = message;
		this.delay = delay;
	}
}
