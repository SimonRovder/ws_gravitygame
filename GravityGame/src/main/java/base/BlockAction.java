package base;


public class BlockAction {
	private boolean repeatable;
	private boolean performed;
	private int[] actionData;
	private int actionId;
	
	public BlockAction(boolean repeatable, int[] actionData, int actionId) {
		this.repeatable = repeatable;
		this.actionData = actionData;
		this.actionId = actionId;
		this.performed = false;
	}
	
	public void performAction(Level l, Resources r){
		try{
			if(!this.performed || this.repeatable){
				this.performed = true;
				switch (this.actionId){
				case 0:
					BasicBlock b = r.getBlockById(this.actionData[1]);
					for(MappedBlock m : l.blocks){
						if(m.referenceId == this.actionData[0]){
							m.block = b;
						}
					}
					break;
				case 1:
					BasicBlock b1 = r.getBlockById(this.actionData[1]);
					BasicBlock b2 = r.getBlockById(this.actionData[2]);
					for(MappedBlock m : l.blocks){
						if(m.referenceId == this.actionData[0]){
							if(m.block == b1){
								m.block = b2;
							}else if(m.block == b2){
								m.block = b1;
							}
							
						}
					}
					break;
				default:
					GravityGame.console.addReport("Invalid Action tried to take place!");
				}
				r.switcher.play();
			}
		}catch(Exception e){
			GravityGame.console.addReport("Impossible Action tried to take place!");
		}
	}
	
	public boolean isRepeatable() {
		return repeatable;
	}

	public void setRepeatable(boolean repeatable) {
		this.repeatable = repeatable;
	}

	public int getActionData(int i) {
		return actionData[i];
	}

	public void setActionData(int[] actionData) {
		this.actionData = actionData;
	}

	public int getActionId() {
		return actionId;
	}

	public void setActionId(int actionId) {
		this.actionId = actionId;
	}

	public String toString(){
		String ret;
		if(this.repeatable){
			ret = "1";
		}else{
			ret = "0";
		}
		for(int i : this.actionData){
			ret = ret + ";;;" + i;
		}
		ret = ret + ";;;" + actionId;
		return ret;
	}
	
	public static BlockAction parseAction(String act) {
		String[] arr = act.split(";;;");
		if(arr.length > 1){
			boolean rep = (Integer.parseInt(arr[0]) == 1);
			int[] actionData = new int[6];
			for(int i = 0; i < 6; i++){
				actionData[i] = Integer.parseInt(arr[i+1]);
			}
			int actId = Integer.parseInt(arr[7]);
			return new BlockAction(rep, actionData, actId);
		}
		return null;
	}
	
	
}