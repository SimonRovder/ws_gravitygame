package base;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class MyBasicState extends BasicGameState{

	protected int id;
	protected Resources resources;
	
	public MyBasicState(int i, Resources r) {
		super();
		this.id = i;
		this.resources = r;
	}
	
	public int getID() {
		return this.id;
	}

	
	
	
	public void init(GameContainer arg0, StateBasedGame arg1) throws SlickException {
		
	}

	public void render(GameContainer arg0, StateBasedGame arg1, Graphics g) throws SlickException {
		GravityGame.console.render(g);
	}

	public void update(GameContainer arg0, StateBasedGame arg1, int arg2) throws SlickException {
		GravityGame.console.update();
	}
}
