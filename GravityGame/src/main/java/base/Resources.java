package base;

import java.io.File;
import java.util.LinkedList;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JFileChooser;

import org.newdawn.slick.Color;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

public class Resources{

	public static final int blockSize = 64;
	public static final int speed = 4;
	public static final double gravity = 0.5;
	public static final double fallControl = 0.2;
	public static final int playerSize = 32;
	public static final int screeenWidth = 1200;
	public static final int screeenHeight = 700;
	public static final int maxHealth = 1000;
	public static final float healthDecreaseLimit = 20;
	public static final float damageConstant = 10;
	public static final String pathToResources = "src/main/resources/";
	public static final String pathToImages = pathToResources + "images/";
	public static final String pathToMenuButtons = pathToImages + "MenuButtons/";
	public static final String levelExtension = ".ggl";
	public static final String dataExtension = ".ggd";
	
	public static final Color buttonTextColor = new Color(255,255,255);
	public static final int reportLife = 200;
	public static final int consoleSize = 10;
	
	
	public LinkedList<BasicBlock> blocks;
	public Image player;
	public Image background;
	public Image button;
	public Sound menumusic;
	public Sound gamemusic;
	public Sound switcher;
	
	public StoryLineManager slm;
	
	public LinkedList<MySound> sounds;
	
	public Resources() throws SlickException{
		this.background = new Image(pathToResources + "images/background.jpg");
		this.menumusic = new Sound(pathToResources + "sounds/menumusic.ogg");
		this.slm = new StoryLineManager(this);
		this.player = new Image(pathToResources + "images/player.png");
		this.button = new Image(pathToResources + "images/BlockImages/button.png");
		this.gamemusic = new Sound(pathToResources + "sounds/gamemusic.ogg");
		this.switcher = new Sound(pathToResources + "sounds/switch.ogg");
		this.blocks = new LinkedList<BasicBlock>();
		this.blocks.add(new BasicBlock(0, "Air", true, new Image(pathToResources + "images/BlockImages/air.png")));
		this.blocks.add(new BasicBlock(1, "Stone", false, new Image(pathToResources + "images/BlockImages/stone.png")));
		this.blocks.add(new BasicBlock(2, "Emitter", true, new Image(pathToResources + "images/BlockImages/emitter.png")));
		Image[] torch = {new Image(pathToResources + "images/BlockImages/Torch/torch1.png"),
				new Image(pathToResources + "images/BlockImages/Torch/torch2.png"),
				new Image(pathToResources + "images/BlockImages/Torch/torch3.png"),
				new Image(pathToResources + "images/BlockImages/Torch/torch4.png")};
		
		this.blocks.add(new AnimatedBlock(3, "Torch", true, torch, 6));
		
		Image[] shield = {new Image(pathToResources + "images/BlockImages/Shield/shield1.png"),
				new Image(pathToResources + "images/BlockImages/Shield/shield2.png"),
				new Image(pathToResources + "images/BlockImages/Shield/shield3.png"),
				new Image(pathToResources + "images/BlockImages/Shield/shield4.png")};
		
		this.blocks.add(new AnimatedBlock(4, "Force Field", false, shield, 4));
		
		Image[] fryer = {new Image(pathToResources + "images/BlockImages/Fryer/fryer1.png"),
				new Image(pathToResources + "images/BlockImages/Fryer/fryer2.png"),
				new Image(pathToResources + "images/BlockImages/Fryer/fryer3.png"),
				new Image(pathToResources + "images/BlockImages/Fryer/fryer4.png")};
		this.blocks.add(new EffectBlock(5, "Fryer", true, fryer, 20, true){
			public void handleEffect(Player p){
				p.changeHealth(-10);
			}
		});
		
		Image[] catcher = {new Image(pathToResources + "images/BlockImages/FallCatcher/FC1.png"),
				new Image(pathToResources + "images/BlockImages/FallCatcher/FC2.png")};
		this.blocks.add(new EffectBlock(6, "Fall Breaker", true, catcher, 50, true){
			public void handleEffect(Player p){
				int abs = (int) Math.sqrt(p.dx*p.dx+p.dy+p.dy);
				if(abs > 0){
					p.dx = p.dx/abs;
					p.dy = p.dy/abs;
				}
			}
		});
		
		Image[] exit = {new Image(pathToResources + "images/BlockImages/Exit/exit1.png"),
				new Image(pathToResources + "images/BlockImages/Exit/exit2.png"),
				new Image(pathToResources + "images/BlockImages/Exit/exit3.png"),
				new Image(pathToResources + "images/BlockImages/Exit/exit4.png")};
		
		this.blocks.add(new AnimatedBlock(7, "Exit", true, exit, 4));
		
		
		for(BasicBlock b : this.blocks){
			if(b instanceof AnimatedBlock){
				for(Image i : ((AnimatedBlock) b).frames){
					i.setCenterOfRotation(blockSize/2, blockSize/2);
				}
			}else{
				b.image.setCenterOfRotation(blockSize/2, blockSize/2);
			}
		}
		
		this.sounds = new LinkedList<MySound>();
	}
	
	public void refresh(){
		for(BasicBlock b : this.blocks){
			if(b instanceof AnimatedBlock) ((AnimatedBlock) b).nextFrame();
		}
	}
	
	public FadeOutTransition getFOT(){
		return new FadeOutTransition(Color.black, 200);
	}
	
	public FadeInTransition getFIT(){
		return new FadeInTransition(Color.black, 200);
	}
	
	public BasicBlock getBlockById(int id){
		for(BasicBlock b : this.blocks){
			if(b.id == id) return b;
		}
		return null;
	}

	public static int toGrid(int a) {
		if(a >= 0) return a/blockSize;
		return a/blockSize - 1;
	}

	public static int toBlockCenter(int i) {
		return i*blockSize + (blockSize/2);
	}
	
	public static String getPath(){
		JButton b = new JButton();
		JFileChooser chooser = new JFileChooser();
		if(chooser.showOpenDialog(b) == JFileChooser.APPROVE_OPTION){
			return chooser.getSelectedFile().getAbsolutePath();
		}
		return null;
	}

	public MySound getSound(String name) {
		for(MySound s : this.sounds){
			if(s.name.equals(name)){
				return s;
			}
		}
		return null;
	}

	public void loadSounds(String link) throws Exception {
		GravityGame.console.addReport("LOADING SOUNDS: ");
		this.sounds = new LinkedList<MySound>();
		File folder = new File(link + "/SOUNDS");
		File[] listOfFiles = folder.listFiles();
		for (File f : listOfFiles){
			if("ogg".equals(getExtension(f.getAbsolutePath()))){
				GravityGame.console.addReport("Loading sound file: " + f.getAbsolutePath());
				this.sounds.add(new MySound(f.getAbsolutePath(), getFileName(f.getAbsolutePath())));
			}
		}
	}

	private static String getFileName(String path){
		String[] arr = path.split(Pattern.quote("/"));
		arr = arr[arr.length - 1].split(Pattern.quote("."));
		return arr[0];
	}
	
	private static String getExtension(String path) {
		String[] arr = path.split(Pattern.quote("."));
		return arr[arr.length - 1];
	}
}