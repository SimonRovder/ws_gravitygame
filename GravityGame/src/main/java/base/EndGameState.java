package base;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

public class EndGameState extends MyBasicState{

	private MyButton mm;
	
	public EndGameState(int i, Resources r) {
		super(i, r);
	}
	
	public void enter(GameContainer container, StateBasedGame game) {
		
	}
	
	public void init(GameContainer c, StateBasedGame s) throws SlickException {
		this.mm = new MyButton(50,100,100,50, "BACK TO MAIN MENU");
	}

	public void render(GameContainer c, StateBasedGame s, Graphics g) throws SlickException {
		g.drawImage(this.resources.background, 0, 0);
		g.setColor(Color.green);
		g.drawString("CONGRATULATIONS! You have won the game!", 50, 50);
		this.mm.render(g);
		super.render(c,s,g);
	}
	
	public void update(GameContainer c, StateBasedGame s, int arg) throws SlickException {
		super.update(c,s,arg);
		int x = c.getInput().getMouseX();
		int y = c.getInput().getMouseY();
		this.mm.updateRange(x, y);
		if(c.getInput().isMousePressed(Input.MOUSE_LEFT_BUTTON)){
			if(this.mm.isInRange()){
				s.enterState(0,null,null);
			}
		}
	}
	
}
