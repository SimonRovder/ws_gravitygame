package base;

import java.util.LinkedList;


public class StoryLineManager{
	private Resources r;
	
	private String link;
	private LinkedList<Level> levels;
	private Reader reader;
	private Appender appender;
	private String gameName;
	private MyButton gameButton;
	
	public StoryLineManager(Resources r){
		this.r = r;
		this.link = "";
		this.reader = new Reader();
		this.appender = new Appender();
		this.levels = null;
	}

	public String getLink(){
		return this.link;
	}

	public void saveAll() throws Exception{
		this.appender.open(this.link + "/data" + Resources.dataExtension, false);
		for(Level l : this.levels){
			this.appender.write(l.getPath());
			l.saveLevelToFile();
		}
	}
//
//	public Level getNextLevel(Level current) throws Exception{
//		if(current == null){
//			if(this.levels.size() > 0){
//				return this.levels.get(0);
//			}else{
//				throw new Exception("No Levels");
//			}
//		}
//		int i = this.levels.indexOf(current);
//		if (i < this.levels.size()){
//			return this.levels.get(i+1);
//		}else{
//			throw new Exception("Last level.");
//		}
//	}
//
//	public Level getPreviousLevel(Level current) throws Exception{
//		int i = this.levels.indexOf(current);
//		if (i > 0){
//			return this.levels.get(i-1);
//		}else{
//			throw new Exception("First level.");
//		}
//	}

	public void loadStory(String link) throws Exception{
		this.link = link;
		this.r.loadSounds(link);
		GravityGame.console.addReport("LOADING LEVELS: ");
		this.levels = new LinkedList<Level>();
		this.reader.openFile(this.link + "/data" + Resources.dataExtension);
		this.gameName = this.reader.nextLine();
		this.gameButton.setText(this.gameName);
		String temp = this.reader.nextLine();
		while(temp != null){
			GravityGame.console.addReport("Loading level: " + temp);
			this.levels.add(new Level(this.r, this.link + "/" + temp + Resources.levelExtension));
			temp = this.reader.nextLine();
		}
		if(this.levels.size() == 0){
			throw new Exception("Empty story line.");
		}
	}

	public void setGameButton(MyButton gameButton){
		this.gameButton = gameButton;
		this.gameButton.setText(this.gameName);
	}

	public Level getLevel(int i) {
		try{
			return this.levels.get(i);
		}catch(Exception ex){
			return null;
		}
	}
}