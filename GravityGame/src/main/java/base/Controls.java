package base;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

public class Controls extends MyBasicState{
	
	private MyButton back;
	private static Image controlsImage;
	
	
	
	public Controls(int i, Resources r) {
		super(i,r);
	}

	public void init(GameContainer c, StateBasedGame s) throws SlickException {
		Controls.controlsImage = new Image(Resources.pathToImages + "controls.png");
		int width = 512;
		int height = 64;
		int offsetLeft = (Resources.screeenWidth / 2) - (width/2);
		this.back = new MyButton(offsetLeft, 600, width, height, "Main Menu");
	}

	public void render(GameContainer c, StateBasedGame s, Graphics g) throws SlickException {
		g.drawImage(this.resources.background, 0, 0);
		g.drawImage(Controls.controlsImage, Resources.screeenWidth/2 - 256, 20);
		this.back.render(g);
		super.render(c,s,g);
		
	}
	
	public void update(GameContainer c, StateBasedGame s, int arg) throws SlickException {
		super.update(c,s,arg);
		int x = c.getInput().getMouseX();
		int y = c.getInput().getMouseY();
		this.back.updateRange(x, y);
		
		if(c.getInput().isKeyPressed(Input.KEY_ESCAPE)){
			s.enterState(0,null,null);
		}
		
		if(c.getInput().isMousePressed(Input.MOUSE_LEFT_BUTTON)){
			if(this.back.isInRange()){
				s.enterState(0,null,null);
			}
		}
	}
}