package base;


import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

public class SplashState extends MyBasicState{

	private Image background;
	private int delay = 10;
	
	public SplashState(int i, Resources r) {
		super(i, r);
	}
	
	public void init(GameContainer c, StateBasedGame s) throws SlickException {
		this.background = new Image(Resources.pathToImages + "/splash.jpg");
	}

	public void render(GameContainer c, StateBasedGame s, Graphics g) throws SlickException {
		g.drawImage(this.background, 0, 0);
		super.render(c,s,g);
		this.delay--;
	}
	
	public void update(GameContainer c, StateBasedGame s, int arg) throws SlickException {
		super.update(c,s,arg);
		if(this.delay < 0){
			s.enterState(0);
		}
	}
	
}
