package base;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class MyButton {
	public int x;
	public int y;
	public int width;
	public int height;
	private boolean inRange;
	private final Color offBack = Color.orange;
	private final Color onBack = Color.yellow;
	private final Color offText = Color.black;
	private final Color onText = Color.black;
	private String text;
	
	public MyButton(int x, int y, int width, int height, String text){
		this.text = text;
		this.x = x;
		this.y = y;
		this.height = height;
		this.width = width;
		this.inRange = false;
	}
	
	public void render(Graphics g){
		if(this.inRange){
			g.setColor(this.onBack);
			g.fillRect(x, y, width, height);
			g.setColor(this.onText);
			g.drawString(this.text, this.x + 10, this.y + 10);
		}else{
			g.setColor(this.offBack);
			g.fillRect(x, y, width, height);
			g.setColor(this.offText);
			g.drawString(this.text, this.x + 10, this.y + 10);
		}
	}
	
	public void updateRange(int x, int y){
		this.inRange = false;
		if(this.x <= x && (this.x+this.width) >= x){
			if(this.y <= y && (this.y+this.height) >= y){
				this.inRange = true;
			}
		}
	}
	
	public boolean isInRange(){
		return this.inRange;
	}

	public void setText(String gameName) {
		this.text = "Play Game: " + gameName;
	}
}
