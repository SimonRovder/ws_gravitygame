package base;

import java.util.LinkedList;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Color;


public class Console {
	private LinkedList<ConsoleReport> reports;
	
	public Console(){
		this.reports = new LinkedList<ConsoleReport>();
	}
	
	public void addReport(String message){
		System.out.println(message);
		this.reports.add(new ConsoleReport(Resources.reportLife, message));
		if(this.reports.size() > Resources.consoleSize){
			this.reports.remove(0);
		}
	}
	
	public void render(Graphics g){
		g.setColor(Color.white);
		int a = 0;
		try{
			for(ConsoleReport cr : this.reports){
				g.drawString(cr.message, 10, 30 + a*15);
				a++;
			}
		}catch(Exception ex){
			
		}
	}

	public void update() {
		ConsoleReport cr;
		int i = 0;
		while(this.reports.size() > i){
			cr = this.reports.get(i);
			cr.delay--;
			if(cr.delay < 0){
				this.reports.remove(i);
				i--;
			}
			i++;
		}
	}
}
