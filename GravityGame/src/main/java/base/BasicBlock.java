package base;

import org.newdawn.slick.Image;

public class BasicBlock {
	public int id;
	public boolean passable;
	public Image image;
	public String name;
	
	public BasicBlock(int id, String name, boolean passable, Image image) {
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.id = id;
		this.passable = passable;
		this.image = image;
		this.name = name;
	}
}
