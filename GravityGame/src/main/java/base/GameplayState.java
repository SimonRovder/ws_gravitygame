package base;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

public class GameplayState extends MyBasicState{
	private Level level;
	private Player player;
	private boolean justJumped;
	private boolean cheatMode;
	private boolean showData;

	private MappedBlockEditor mbe;
	
	private BasicBlock blockToPlace;
	private int blockToPlacei;

	private int lastX;
	private int lastY;
	private BasicBlock nextBlock;
	private Input input;
	private byte compass;
	
	private int currentLevel;
	
	public GameplayState(int i, Resources r) throws Exception {
		super(i,r);
		this.mbe = new MappedBlockEditor(r);
	}
	
	public void init(GameContainer c, StateBasedGame s) throws SlickException {
		this.input = c.getInput();
		this.blockToPlacei = 0;
		this.currentLevel = 0;
	}
	
	public void enter(GameContainer container, StateBasedGame game) {
		if(this.level == null ){
			this.loadCurrentLevel(false, game);
		}
	}
	
	private void loadCurrentLevel(boolean inter, StateBasedGame game){
		Level l = this.resources.slm.getLevel(this.currentLevel);
		if(l == null){
			this.currentLevel = 0;
			this.loadCurrentLevel(false, game);
			game.enterState(5);
		}else{
			this.level = l;
			this.player = new Player(l.getStartX(),l.getStartY(),l.getStartX()-Resources.blockSize-5,l.getStartY()-Resources.blockSize-5,0,0,true,1000);
			this.resetPlayerPosition();
			this.player.resetValues();
			this.cheatMode = false;
			this.justJumped = false;
			this.showData = false;
			this.rotateAbsolute('s');
			this.checkFalling();
			if(inter){
				game.enterState(3);
			}
		}
	}

	public void render(GameContainer c, StateBasedGame s, Graphics g) throws SlickException {
		g.drawImage(this.resources.background, 0, 0);
		this.level.render(g, this.player.x, this.player.y, this.cheatMode);
		g.drawImage(this.resources.player, (Resources.screeenWidth/2 - (Resources.playerSize/2)), (Resources.screeenHeight/2 - (Resources.playerSize/2)));
		g.setColor(Color.white);
		if(this.showData){
			g.drawString("X: " + this.player.x + " Y: " + this.player.y, 20, 50);
			g.drawString("GridX: " + Resources.toGrid(this.player.x) + " GridY: " + Resources.toGrid(this.player.y), 20, 70);
			g.drawString("Net Motion: " + (this.player.dy + this.player.dx), 20, 90);
			g.drawString("Compass: " + this.compass, 20, 110);
		}
		g.setColor(Color.lightGray);
		g.drawString("Health: " + this.player.getHealth() + " / " + Resources.maxHealth, 20, Resources.screeenHeight - 55);
		g.drawRect(20,  Resources.screeenHeight - 35, 202, 5);
		g.fillRect(21,  Resources.screeenHeight - 34, this.player.getHealth()/5, 3);
		if(this.cheatMode){
			g.setColor(Color.red);
			g.drawRect(Resources.screeenWidth/2 + (-this.player.x) + Resources.toGrid(getMouseXOnScreen())*Resources.blockSize, Resources.screeenHeight/2 + (-this.player.y) + Resources.toGrid(getMouseYOnScreen())*Resources.blockSize, Resources.blockSize, Resources.blockSize);
			g.setColor(Color.white);
			this.renderEditor(g);
		}
		g.setColor(Color.white);
		super.render(c,s,g);
	}
	
	private void renderEditor(Graphics g){
		g.fillRect(Resources.screeenWidth - 30 - Resources.blockSize, Resources.screeenHeight - 30 - Resources.blockSize, Resources.blockSize + 20, Resources.blockSize + 20);
		g.drawImage(this.blockToPlace.image, Resources.screeenWidth - 20 - Resources.blockSize, Resources.screeenHeight - 20 - Resources.blockSize);
	}

	private void checkGravityRotation(){
		if(this.input.isKeyPressed(Input.KEY_LEFT)){
			this.rotate(3);
		}else if(this.input.isKeyPressed(Input.KEY_UP)){
			this.rotate(2);
		}else if(this.input.isKeyPressed(Input.KEY_RIGHT)){
			this.rotate(1);
		}
	}
	
	private void checkJump(){
		if(this.justJumped){
			this.justJumped = false;
		}else{
			this.checkFalling();
		}
	}
	
	private void cheatModeControl(){

		if(this.input.isKeyPressed(Input.KEY_R)){
			this.resetPlayerPosition();
		}
		if(this.input.isKeyPressed(Input.KEY_F8)){
			this.level.setStartX(Resources.toGrid(this.player.x));
			this.level.setStartY(Resources.toGrid(this.player.y));
		}
		
		if(this.input.isKeyDown(Input.KEY_A)){
			this.player.x -= Resources.speed;
		}
		if(this.input.isKeyDown(Input.KEY_F7)){
			for(MappedBlock m : this.level.blocks){
				if(m.sound != null){
					m.played = false;
				}
			}
		}
		if(this.input.isKeyDown(Input.KEY_D)){
			this.player.x += Resources.speed;
		}
		if(this.input.isKeyDown(Input.KEY_S)){
			this.player.y += Resources.speed;
		}
		if(this.input.isKeyDown(Input.KEY_W)){
			this.player.y -= Resources.speed;
		}
		if(this.input.isKeyDown(Input.KEY_J)){
			this.player.x -= Resources.speed*30;
		}
		if(this.input.isKeyDown(Input.KEY_L)){
			this.player.x += Resources.speed*30;
		}
		if(this.input.isKeyDown(Input.KEY_K)){
			this.player.y += Resources.speed*30;
		}
		if(this.input.isKeyDown(Input.KEY_I)){
			this.player.y -= Resources.speed*30;
		}
		if(this.input.isKeyPressed(Input.KEY_F6)){
			try {
				this.level.saveLevelToFile();
				GravityGame.console.addReport("Level Saved.");
			} catch (Exception e) {
				GravityGame.console.addReport("Could not save Level.");
			}
		}
		if(this.input.isMousePressed(Input.MOUSE_LEFT_BUTTON)){
			this.placeBlock();
		}
		if(this.input.isMousePressed(Input.MOUSE_RIGHT_BUTTON)){
			this.removeBlock();
		}
		if(this.input.isKeyPressed(Input.KEY_Q)){
			MappedBlock mb = this.level.getMappedBlockByCoordinates(getMouseXOnScreen(), getMouseYOnScreen());
			if(mb != null){
				this.mbe.setVisible(true);
				this.mbe.loadMappedBlock(mb);
				this.input.pause();
				this.input.resume();
			}else{
				GravityGame.console.addReport("There is no block there!");
			}
		}
	}
	
	private int getMouseXOnScreen(){
		return this.player.x - Resources.screeenWidth/2 + this.input.getMouseX();
	}
	
	private int getMouseYOnScreen(){
		return this.player.y - Resources.screeenHeight/2 + this.input.getMouseY();
	}
	
	private void removeBlock() {
		this.level.deleteBlock(this.getMouseXOnScreen(), this.getMouseYOnScreen());
	}

	private void placeBlock(){
		this.level.setBlock(this.getMouseXOnScreen(), this.getMouseYOnScreen(), this.blockToPlace);
	}
	
	private void normalControl(){

		if(this.input.isKeyPressed(Input.KEY_ENTER)){
			this.level.checkAction(this.player.x, this.player.y);
		}
		
		if(this.player.falling){
			if((this.player.dy + Resources.gravity) < Resources.blockSize){
				this.player.dy += Resources.gravity;
			}
			
			if(this.input.isKeyDown(Input.KEY_A)){
				if(Math.abs(this.player.dx - Resources.fallControl) < Resources.speed){
					this.player.dx -= Resources.fallControl;
				}
			}
			if(this.input.isKeyDown(Input.KEY_D)){
				if(Math.abs(this.player.dx + Resources.fallControl) < Resources.speed){
					this.player.dx += Resources.fallControl;
				}
			}
			
			this.player.nextX = this.player.x + (int)this.player.dx;
			this.player.nextY = this.player.y + (int)this.player.dy;
			
		}else{
			this.player.dx = 0;
			if(this.input.isKeyPressed(Input.KEY_A)){
				this.player.dx = -Resources.speed;
			}
			if(this.input.isKeyPressed(Input.KEY_D)){
				this.player.dx = Resources.speed;
			}
			
			if(this.input.isKeyDown(Input.KEY_SPACE)){
				this.player.falling = true;
				this.player.dy = -10;
				this.justJumped = true;
			}else{
				if(this.input.isKeyDown(Input.KEY_A)){
					this.player.nextX = this.player.x - Resources.speed;
				}
				if(this.input.isKeyDown(Input.KEY_D)){
					this.player.nextX = this.player.x + Resources.speed;
				}
			}
		}
		if(this.player.nextX != this.player.x || this.player.nextY != this.player.y){
			this.movePlayer();
		}
	}
	
	private void checkOtherInput(){
		if(this.input.isKeyPressed(Input.KEY_F5)){
			this.showData = !this.showData;
		}
		if(this.input.isKeyPressed(Input.KEY_C)){
			this.cheatMode = !this.cheatMode;
			this.blockToPlace = this.resources.blocks.get(0);
			if(this.cheatMode){
				GravityGame.console.addReport("Build mode has been activated.");
			}else{
				GravityGame.console.addReport("Build mode has been deactivated.");
			}
		}
	}
	
	private void resetPlayerPosition(){
		this.player.x = this.level.getStartX()*Resources.blockSize + Resources.blockSize/2;
		this.player.y = this.level.getStartY()*Resources.blockSize + Resources.blockSize/2;
	}
	
	public void mouseWheelMoved(int dw){
		if(dw < 0){
			dw = -1;
		}else{
			dw = 1;
		}
		this.blockToPlacei = (this.blockToPlacei + dw + this.resources.blocks.size()) % (this.resources.blocks.size());
		this.blockToPlace = this.resources.blocks.get(this.blockToPlacei);
	}
	
	public void update(GameContainer c, StateBasedGame s, int arg) throws SlickException {
		super.update(c,s,arg);
		this.resources.refresh();
		this.checkGravityRotation();
		this.player.nextX = this.player.x;
		this.player.nextY = this.player.y;
		this.checkJump();
		if(this.cheatMode){
			this.cheatModeControl();
		}else{
			this.normalControl();
			this.nextBlock = this.level.getBlockByCoordinates(this.player.x, this.player.y);
			if(this.nextBlock != null){
				if(!this.nextBlock.passable){
					this.player.changeHealth(-50);
				}
				if(this.nextBlock instanceof EffectBlock){
					if(((EffectBlock) this.nextBlock).recurring || (this.nextBlock != this.level.getBlockByCoordinates(this.lastX, this.lastY))){
						((EffectBlock) this.nextBlock).handleEffect(this.player);
					}
				}
				if(this.nextBlock.id == 7){
					this.currentLevel++;
					this.loadCurrentLevel(true, s);
				}
			}
		}
		try{
			this.level.getMappedBlockByCoordinates(this.player.x, this.player.y).playSound();
		}catch(NullPointerException e){
			
		}catch(Exception e){
			GravityGame.console.addReport(e.getMessage());
		}
		this.checkOtherInput();
		if(this.input.isKeyPressed(Input.KEY_ESCAPE)){
			resources.gamemusic.stop();
			resources.menumusic.loop();
			s.enterState(0, this.resources.getFOT(), this.resources.getFIT());
		}
		this.lastX = this.player.x;
		this.lastY = this.player.y;
		this.input.clearKeyPressedRecord();
		if(this.player.getHealth() <= 0){
			s.enterState(4);
		}
	}
	
	public void rotateAbsolute(char o){
		switch (o){
		case 'n' : this.rotate((this.compass+2) % 4); break;
		case 's' : this.rotate(this.compass); break;
		case 'e' : this.rotate((this.compass+1) % 4); break;
		case 'w' : this.rotate((this.compass+3) % 4); break;
		}
	}
	
	private void rotate(int i) {
		this.compass = (byte) ((this.compass + -i + 4)%4);
		if(i == 0) return;
		Transformator.transform(this.resources, this.player, this.level, (i-1));
	}

	private void movePlayer(){
		this.player.updatePointNetX();
		if(this.checkFree()){
			this.player.x = this.player.nextX;
		}else{
			this.player.dx = 0;
			if(this.player.nextX > this.player.x){
				this.player.x = (Resources.toGrid(this.player.x) + 1) * Resources.blockSize - (Resources.playerSize/2+1);
			}else{
				this.player.x = (Resources.toGrid(this.player.x)) * Resources.blockSize + (Resources.playerSize/2+1);
			}
		}
		this.player.updatePointNetY();
		if(this.checkFree()){
			this.player.y = this.player.nextY;
		}else{
			if(this.player.nextY > this.player.y){
				this.player.y = (Resources.toGrid(this.player.y) + 1) * Resources.blockSize - (Resources.playerSize/2+1);
			}else{
				this.player.dy = 0;
				this.player.y = (Resources.toGrid(this.player.y)) * Resources.blockSize + (Resources.playerSize/2+1);
			}
		}
		checkFalling();
	}
	
	private void checkFalling() {
		this.player.updatePointNetX();
		this.player.updatePointNetY();
		if(this.player.falling){
			if((!this.level.isPassable(this.player.net.dl.x, this.player.net.dl.y + 2)) || 
					(!this.level.isPassable(this.player.net.dr.x, this.player.net.dr.y + 2))){
				this.endFall();
			}
		}else{
			if((this.level.isPassable(this.player.net.dl.x, this.player.net.dl.y + 2)) && 
					(this.level.isPassable(this.player.net.dr.x, this.player.net.dr.y + 2))){
				this.player.falling = true;
			}
		}
	}
	
	private void endFall() {
		int netMotion = (int) (Math.abs(this.player.dx) + Math.abs(this.player.dy));
		if(netMotion > Resources.healthDecreaseLimit){
			this.player.changeHealth((int) (-netMotion * Resources.damageConstant));
		}
		this.player.dx = 0;
		this.player.dy = 0;
		this.player.falling = false;
	}
	
	private boolean checkFree(){
		return 
			this.level.isPassable(this.player.net.ur.x, this.player.net.ur.y) &&
			this.level.isPassable(this.player.net.ul.x, this.player.net.ul.y) &&
			this.level.isPassable(this.player.net.dr.x, this.player.net.dr.y) &&
			this.level.isPassable(this.player.net.dl.x, this.player.net.dl.y);
	}
}
