package base;

public class Player {
	public int x,y,nextX,nextY;
	public float dy,dx;
	public boolean falling;

	private int health;
	
	public PointNet net;
	
	public Player(int x, int y, int nextX, int nextY, int dy, int dx, boolean falling, int health) {
		this.x = x;
		this.y = y;
		this.nextX = nextX;
		this.nextY = nextY;
		this.dy = dy;
		this.health = health;
		this.dx = dx;
		this.falling = falling;
		this.net = new PointNet();
	}
	
	public void updatePointNetX(){
		this.net.dl.x = nextX-Resources.playerSize/2;
		this.net.dr.x = nextX+Resources.playerSize/2;
		this.net.ul.x = nextX-Resources.playerSize/2;
		this.net.ur.x = nextX+Resources.playerSize/2;
		this.net.dl.y = y+Resources.playerSize/2;
		this.net.dr.y = y+Resources.playerSize/2;
		this.net.ul.y = y-Resources.playerSize/2;
		this.net.ur.y = y-Resources.playerSize/2;
	}
	
	public void updatePointNetY(){
		this.net.dl.y = nextY+Resources.playerSize/2;
		this.net.dr.y = nextY+Resources.playerSize/2;
		this.net.ul.y = nextY-Resources.playerSize/2;
		this.net.ur.y = nextY-Resources.playerSize/2;
		this.net.dl.x = x-Resources.playerSize/2;
		this.net.dr.x = x+Resources.playerSize/2;
		this.net.ul.x = x-Resources.playerSize/2;
		this.net.ur.x = x+Resources.playerSize/2;
	}
	
	public int getHealth(){
		return this.health;
	}
	
	public void changeHealth(int change){
		this.health += change;
		if(this.health < 0) this.health = 0;
		if(this.health > 1000) this.health = 1000;
	}

	public void resetValues() {
		this.dx = 0;
		this.dy = 0;
		this.health = Resources.maxHealth;
	}

	public void reset() {
		this.dx = 0;
		this.dy = 0;
		this.health = Resources.maxHealth;
	}
}
