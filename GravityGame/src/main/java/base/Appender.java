package base;

import java.io.*;

/**
 * This class handles all writing to sequential files.
 * 
 * @author Simon Rovder, Spojena skola Novohradska
 * @version 1.0 28 January 2013
 * 
 * HighwayGuru
 * IDE Eclipse 3.7.2
 * Platform: PC
 *
 */

public class Appender
{
	// The BufferedWriter - Variable used to write to a sequential file.
    private BufferedWriter outBuffer;
    
    /**
     * Simple constructor - Create the instance of the class.
     */
    public Appender() {}
    
    /**
     * Opens a sequential file.
     * @param fileName The directory of the file that is to be opened.
     * @param appendBool Boolean determining the way the file will be edited. (true -> append)(false -> rewrite)
     * @throws Exception
     */
    public void open(String fileName, boolean appendBool) throws Exception{
    	// Open the file.
        outBuffer = new BufferedWriter(new FileWriter(fileName, appendBool));
    }
    
    /**
     * Write a line to the end of the opened file.
     * @param text String containing the text that is to be added to the end of the file.
     * @throws Exception
     */
    public void write(String text) throws Exception{
        outBuffer.write(text); 	// Write the text.
        outBuffer.newLine();	// Insert a line break.
    }
    
    /**
     * Closes the opened file. 
     * @throws Exception
     */
    public void close() throws Exception{
        outBuffer.flush();	// Write all the text in the buffer to the file before closing.
        outBuffer.close();	// Close the file.
    }
}