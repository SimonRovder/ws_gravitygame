package base;

import org.newdawn.slick.Image;

public abstract class EffectBlock extends AnimatedBlock{
	public boolean recurring;
	
	public EffectBlock(int id, String name,  boolean passable, Image[] images, int delay, boolean recurring) {
		super(id, name, passable, images, delay);
		this.recurring = recurring;
	}

	public void handleEffect(Player p){}
}