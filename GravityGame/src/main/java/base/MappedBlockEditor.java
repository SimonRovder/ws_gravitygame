package base;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;



@SuppressWarnings("serial")
public class MappedBlockEditor extends JFrame implements ActionListener{

	private JTabbedPane tabs;
	private JPanel blockEditor;
	private JPanel actionEditor;
	
	private JButton done;
	private JButton cancel;
	
	private JTextField referenceId;
	private JCheckBox hasAction;
	
	
	private JTextField[] actionData;
	private JTextField actionType;
	private JCheckBox repeatable;
	
	private JButton refreshList;
	private JTextField currentSound;
	private JCheckBox playSound;
	private JButton setSound;
	private JScrollPane scroller;
	
	private MappedBlock mappedBlock;
	
	private JTable soundSelection;
	
	private Resources resources;
//
//	
//	public static void main(String args[]){
//		MappedBlockEditor mbe = new MappedBlockEditor();
//		mbe.setVisible(true);
//	}
	
	public MappedBlockEditor(){
		super("Block Editor");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLayout(null);
		this.setSize(500,500);
		this.setUpComponents();
		this.setVisible(false);		
	}
	
	public MappedBlockEditor(Resources r){
		super("Block Editor");
		this.resources = r;
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.setLayout(null);
		this.setSize(500,500);
		this.setUpComponents();
		this.setVisible(false);		
	}
	
	private void setUpComponents(){
		this.blockEditor = new JPanel();
		this.blockEditor.setLayout(null);
		this.actionEditor = new JPanel();
		this.actionEditor.setLayout(null);
		JPanel be = this.blockEditor;
		JPanel ae = this.actionEditor;
		
		
		
		myLabel(10,10,180,20,"Reference ID:", be);
		this.referenceId = myTextField(200, 10, 180, 20, be);

		myLabel(10,40,180,20,"Sound Settings:", be);
		this.playSound = myCheckBox(10, 70, 400, 20, "Play Sounds", be);
		
		myLabel(10,100,180,20,"Sound:", be);
		this.currentSound = myTextField(200, 100, 200, 20, be);
		this.currentSound.setEditable(false);
		
		this.refreshList = myButton(20,130,160,20,"Refresh", this,be);
		this.setSound = myButton(200,130,200,20,"Set Sound", this,be);
		
		
		
		
		this.hasAction = myCheckBox(10,10,180,20, "Has an Action", ae);
		
		
		myLabel(10,40,180,20,"Action Type:", ae);
		this.actionType = myTextField(200,40,200,20,ae);
		
		this.repeatable = myCheckBox(10,70,200,20, "Repeatable", ae);
		
		this.actionData = new JTextField[6];
		
		for(int i = 0; i < 6; i++){
			myLabel(10, 100+i*30, 180, 20, "Action Data: " + i, ae);
			this.actionData[i] = myTextField(200, 100+i*30, 200, 20, ae);
		}
		
		this.tabs = new JTabbedPane();
		this.tabs.setBounds(10, 20, 480, 400);
		this.tabs.add("Block Editor", be);
		this.tabs.add("Action Editor", ae);
		this.tabs.setVisible(true);
		this.getContentPane().add(this.tabs);
		
		this.done = myButton(150,440,100,20,"Done",this,(JPanel) this.getContentPane());
		
		this.cancel = myButton(260,440,100,20,"Cancel",this,(JPanel) this.getContentPane());
		
		this.updateSelector();
		this.scroller = new JScrollPane();
		this.blockEditor.add(this.scroller);
	}
	
	
	private void updateSelector(){
		try{
			this.resources.loadSounds(this.resources.slm.getLink());
			this.blockEditor.remove(this.scroller);
			Object[][] sounds = new Object[this.resources.sounds.size()][1];
			for(int i = 0; i < this.resources.sounds.size(); i++){
				sounds[i][0] = this.resources.sounds.get(i).name;
			}
			Object[] o = {"Sounds"};
			this.soundSelection = new JTable(sounds, o){
				public boolean isCellEditable(int row, int column) {                
	                return false;               
				};
			};
			this.soundSelection.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);	
			this.scroller = new JScrollPane(this.soundSelection);
			this.scroller.setBounds(20, 160, 380, 200);
			this.blockEditor.add(this.scroller);
		}catch(Exception ex){
			
		}
	}
	
	private static void myLabel(int x, int y, int width, int height, String text, JPanel p){
		JLabel l = new JLabel();
		l.setText(text);
		l.setBounds(x, y, width, height);
		l.setVisible(true);
		p.add(l);
	}
	
	private static JTextField myTextField(int x, int y, int width, int height, JPanel c){
		JTextField t = new JTextField();
		t.setBounds(x, y, width, height);
		t.setVisible(true);
		c.add(t);
		return t;
	}
	
	private static JButton myButton(int x, int y, int width, int height, String text, ActionListener act, JPanel c){
		JButton button = new JButton();
		button.setText(text);
		button.setBounds(x, y, width, height);
		button.addActionListener(act);
		button.setVisible(true);
		c.add(button);
		return button;
	}
	
	private static JCheckBox myCheckBox(int x, int y, int width, int height, String text, JPanel c){
		JCheckBox box = new JCheckBox();
		box.setText(text);
		box.setBounds(x, y, width, height);
		box.setVisible(true);
		c.add(box);
		return box;
	}
	
	public void loadMappedBlock(MappedBlock m){
		this.mappedBlock = m;
		this.hasAction.setSelected(m.action != null);
		if(m.action != null){
			this.actionType.setText("" + m.action.getActionId());
			this.repeatable.setSelected(m.action.isRepeatable());
			for(int i = 0; i < 6; i++){
				this.actionData[i].setText("" + m.action.getActionData(i));
			}
		}
		this.referenceId.setText("" + m.referenceId);
		this.refreshActivity();
		if(m.sound == null){
			this.playSound.setSelected(false);
			this.currentSound.setText("");
		}else{
			this.playSound.setSelected(true);
			this.currentSound.setText(m.sound.name);
		}
	}
	
	private void refreshActivity(){
		for(int i = 0; i < 6; i++){
			this.actionData[i].setEnabled(this.hasAction.isSelected());
		}
		this.actionType.setEnabled(this.hasAction.isSelected());
		this.repeatable.setEnabled(this.hasAction.isSelected());
	}
	

	private void setTheSound(){
		this.currentSound.setText((this.resources.sounds.get(this.soundSelection.getSelectedRow())).name);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == this.hasAction){
			this.refreshActivity();
		}else if(e.getSource() == this.cancel){
			this.setVisible(false);
		}else if(e.getSource() == this.refreshList){
			this.updateSelector();
		}else if(e.getSource() == this.setSound){
			this.setTheSound();
		}else if(e.getSource() == this.done){
			if(this.save()){
				this.setVisible(false);
			}
		}
	}
	
	private boolean save(){
		try{
			this.mappedBlock.referenceId = Integer.parseInt(this.referenceId.getText());
			if(this.hasAction.isSelected()){
				boolean newRepeatable = this.repeatable.isSelected();
				int actionType = Integer.parseInt(this.actionType.getText());
				int[] data = new int[6];
				for(int i = 0; i < 6; i++){
					data[i] = Integer.parseInt(this.actionData[i].getText());
				}
				BlockAction a = new BlockAction(newRepeatable, data, actionType);
				this.mappedBlock.action = a;				
			}else{
				this.mappedBlock.action = null;
			}
			if(this.playSound.isSelected()){
				this.mappedBlock.sound = this.resources.getSound(this.currentSound.getText());
				this.mappedBlock.played = false;
			}else{
				this.mappedBlock.sound = null;
			}
			return true;
		}catch(Exception e){
			JOptionPane.showMessageDialog(this, "Invalid values.", "Error", JOptionPane.ERROR_MESSAGE);
		}
		return false;
	}
}
