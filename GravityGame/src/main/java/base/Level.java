package base;

import java.util.LinkedList;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class Level {
	public LinkedList<MappedBlock> blocks;
	private Resources resources;
	private Reader reader;
	private Appender appender;
	private String name;
	private String path;
	
	private int startX;
	private int startY;
	
	
	public Level(Resources r, String path) throws Exception{
		this.reader = new Reader();
		this.appender = new Appender();
		this.path = path;
		this.blocks = new LinkedList<MappedBlock>();
		this.resources = r;
		this.readLevelFromFile();
	}
	
	public void saveLevelToFile() throws Exception{
		this.appender.open(this.path, false);
		this.appender.write(this.name);
		this.appender.write("" + this.getStartX());
		this.appender.write("" + this.getStartY());
		for(MappedBlock m : this.blocks){
			this.appender.write(m.toString());
			if(m.action != null){
				this.appender.write(m.action.toString());
			}else{
				this.appender.write("");
			}
		}
		this.appender.close();
	}
	
	private void readLevelFromFile() throws Exception{
		this.reader.openFile(this.path);
		this.name = this.reader.nextLine();
		this.startX = Integer.parseInt(this.reader.nextLine());
		this.startY = Integer.parseInt(this.reader.nextLine());
		String mapped = this.reader.nextLine();
		String act = this.reader.nextLine();
		while(mapped != null && act != null){
			this.blocks.add(MappedBlock.parseMappedBlock(this.resources, mapped, act));
			mapped = this.reader.nextLine();
			act = this.reader.nextLine();
		}
		this.reader.close();
	}
	
	
//	private void addAction(int x, int y, BlockAction action){
//		for(MappedBlock m : this.blocks){
//			if(m.x == x && m.y == y){
//				m.action = action;
//			}
//		}
//	}


	public void render(Graphics g, int x, int y, boolean cheatMode) {
		int xBound = Resources.screeenWidth/2 + Resources.blockSize;
		int yBound = Resources.screeenHeight/2 + Resources.blockSize;
		for(MappedBlock b : this.blocks){
			if(Math.abs((b.x*Resources.blockSize)-x) < xBound && Math.abs((b.y*Resources.blockSize)-y) <yBound){
				g.drawImage(b.block.image, Resources.screeenWidth/2 + (-x) + b.x*Resources.blockSize, Resources.screeenHeight/2 + (-y) + b.y*Resources.blockSize);
				if(b.action != null){
					g.drawImage(this.resources.button, Resources.screeenWidth/2 + (-x) + b.x*Resources.blockSize, Resources.screeenHeight/2 + (-y) + b.y*Resources.blockSize);
				}
				if(cheatMode){
					g.setColor(Color.gray);
					g.drawRect(Resources.screeenWidth/2 + (-x) + b.x*Resources.blockSize + 2, 2 + Resources.screeenHeight/2 + (-y) + b.y*Resources.blockSize, Resources.blockSize-4, Resources.blockSize-4);
					if(b.action != null){
						g.setColor(Color.yellow);
						g.drawRect(Resources.screeenWidth/2 + (-x) + b.x*Resources.blockSize+3, 3+Resources.screeenHeight/2 + (-y) + b.y*Resources.blockSize, Resources.blockSize-6, Resources.blockSize-6);
					}
					if(b.referenceId != 0){
						g.setColor(Color.orange);
						g.drawRect(Resources.screeenWidth/2 + (-x) + b.x*Resources.blockSize+4, 4+Resources.screeenHeight/2 + (-y) + b.y*Resources.blockSize, Resources.blockSize-8, Resources.blockSize-8);
					}
				}
			}
		}
	}

	public boolean isPassable(int x, int y) {
		try{
			return this.getBlockByCoordinates(x, y).passable;
		}catch(NullPointerException e){
			return true;
		}
	}
	
	public BasicBlock getBlockByCoordinates(int x, int y){
		MappedBlock m = this.getMappedBlockByCoordinates(x, y);
		if(m != null) return m.block;
		return null;
	}
	
	public MappedBlock getMappedBlockByCoordinates(int x, int y){
		x = Resources.toGrid(x);
		y = Resources.toGrid(y);
		for(MappedBlock m : this.blocks){
			if(m.x == x && m.y == y){
				return m;
			}
		}
		return null;
	}

	public void setBlock(int x, int y, BasicBlock blockToPlace) {
		x = Resources.toGrid(x);
		y = Resources.toGrid(y);
		for(MappedBlock b: this.blocks){
			if(b.x == x && b.y == y){
				b.block = blockToPlace;
				return;
			}
		}
		this.blocks.add(new MappedBlock(blockToPlace, x, y, 0, null));
	}

	public void deleteBlock(int x, int y) {
		x = Resources.toGrid(x);
		y = Resources.toGrid(y);
		for(MappedBlock b: this.blocks){
			if(b.x == x && b.y == y){
				this.blocks.remove(b);
				return;
			}
		}
	}

	public void checkAction(int x, int y) {
		x = Resources.toGrid(x);
		y = Resources.toGrid(y);
		for(MappedBlock m: this.blocks){
			if(m.x == x && m.y == y && m.action != null){
				m.action.performAction(this, this.resources);
				return;
			}
		}
	}

	public int getStartX() {
		return startX;
	}

	public void setStartX(int startX) {
		this.startX = startX;
	}

	public int getStartY() {
		return startY;
	}

	public void setStartY(int startY) {
		this.startY = startY;
	}
	
	public String getPath(){
		return this.path;
	}
}
