package base;

import javax.swing.JButton;
import javax.swing.JFileChooser;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

public class MainMenu extends MyBasicState{
	
	private MyButton[] buttons;
	
	
	
	public MainMenu(int i, Resources r) {
		super(i,r);
	}
	
	public void init(GameContainer c, StateBasedGame s) throws SlickException {
		this.buttons = new MyButton[3];
		int width = 512;
		int height = 64;
		int offsetTop = 100;
		int offsetLeft = (Resources.screeenWidth / 2) - (width/2);
		this.buttons[0] = new MyButton(offsetLeft, offsetTop, width, height, "Play Game");
		this.buttons[1] = new MyButton(offsetLeft, offsetTop+(height+10), width, height, "Controls");
		this.buttons[2] = new MyButton(offsetLeft, offsetTop+2*(height+10), width, height, "Load Game Folder");
		this.resources.slm.setGameButton(this.buttons[0]);
	}

	public void render(GameContainer c, StateBasedGame s, Graphics g) throws SlickException {
		g.drawImage(this.resources.background, 0, 0);
		for(MyButton b : this.buttons){
			b.render(g);
		}
		super.render(c,s,g);
	}
	
	public void update(GameContainer c, StateBasedGame s, int arg) throws SlickException {
		super.update(c,s,arg);
		int x = c.getInput().getMouseX();
		int y = c.getInput().getMouseY();
		
		for(MyButton b : this.buttons){
			b.updateRange(x, y);
		}
		
		if(c.getInput().isMousePressed(Input.MOUSE_LEFT_BUTTON)){
			if(this.buttons[0].isInRange()){
				resources.menumusic.stop();
				resources.gamemusic.loop();
				s.enterState(1,null,null);
			}
			if(this.buttons[1].isInRange()){
				s.enterState(2,null,null);
			}
			if(this.buttons[2].isInRange()){
				JFileChooser chooser = new JFileChooser();
				chooser.setCurrentDirectory(new java.io.File("."));
				chooser.setDialogTitle("Select Folder");
				chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				chooser.setAcceptAllFileFilterUsed(false);
				chooser.showOpenDialog(new JButton());
				try {
					this.resources.slm.loadStory(chooser.getSelectedFile().getAbsolutePath());
				} catch (Exception e) {
					e.printStackTrace();
					GravityGame.console.addReport("ERROR occured while loading story.");
				}
			}
		}
	}
}
