package base;

import org.newdawn.slick.Image;

public class AnimatedBlock extends BasicBlock{

	public Image[] frames;
	public int current;
	private int delay;
	private int step;
	
	public AnimatedBlock(int id, String name, boolean passable, Image[] images, int delay) {
		super(id, name, passable, images[0]);
		this.frames = images;
		this.delay = delay;
		this.step = 0;
		this.current = 0;
	}
	
	public void nextFrame(){
		this.step++;
		if(this.step == this.delay){
			this.current = (this.current + 1) % this.frames.length;
			this.image = this.frames[this.current];
			this.step = 0;
		}
	}
}
