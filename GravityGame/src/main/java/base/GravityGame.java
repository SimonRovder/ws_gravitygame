package base;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.Transition;

public class GravityGame extends StateBasedGame{
	
	private MyBasicState[] states;
	public Resources r;
	public static Console console;
	
	
	public GravityGame() {
		super("Gravity Game");
		console = new Console();
	}

	@Override
	public void initStatesList(GameContainer c){
		try {
			this.r = new Resources();
			r.menumusic.loop();
			this.states = new MyBasicState[6];
			this.states[0] = new MainMenu(0, this.r);
			this.states[1] = new GameplayState(1, this.r);
			this.states[2] = new Controls(2, this.r);
			this.states[3] = new InterLevelState(3, this.r);
			this.states[4] = new DieState(4, this.r);
			this.states[5] = new EndGameState(5, this.r);
			for(BasicGameState gs : this.states){
				if(gs != null)	this.addState(gs);
			}
		} catch (Exception e) {
			e.printStackTrace();
			console.addReport(e.getMessage());
		}
	}
	
	public void enterState(int id, Transition s, Transition d){
		super.enterState(id, this.r.getFOT(), this.r.getFIT());
	}
	
	public static void main(String[] args){
		AppGameContainer app;
		try {
			app = new AppGameContainer(new GravityGame());
			app.setDisplayMode(Resources.screeenWidth, Resources.screeenHeight, false);
			app.setAlwaysRender(true);
			app.setTargetFrameRate(60);
			app.setVSync(true);
			app.start();
		} catch (SlickException e) {
			console.addReport(e.getMessage());
		}
	}
}