package base;

import java.util.LinkedList;

import org.newdawn.slick.Image;

public class Transformator {
	public static final byte[][][] matrices = {
		{{0,-1},{1,0}},
		{{-1,0},{0,-1}},
		{{0,1},{-1,0}}
	};
	
	public static void transform(Resources r, Player p, Level lev, int trans){
		int x;
		int y;
		float fx;
		float fy;
		LinkedList<MappedBlock> l = lev.blocks;
		for(MappedBlock m : l){
			x = m.x;
			y = m.y;
			m.x = matrices[trans][0][0]*x + matrices[trans][0][1]*y;
			m.y = matrices[trans][1][0]*x + matrices[trans][1][1]*y;
		}
		x = Resources.toGrid(p.x);
		y = Resources.toGrid(p.y);
		p.x = Resources.toBlockCenter(matrices[trans][0][0]*x + matrices[trans][0][1]*y);
		p.y = Resources.toBlockCenter(matrices[trans][1][0]*x + matrices[trans][1][1]*y);
		
		fx = p.dx;
		fy = p.dy;
		p.dx = matrices[trans][0][0]*fx + matrices[trans][0][1]*fy;
		p.dy = matrices[trans][1][0]*fx + matrices[trans][1][1]*fy;
		
		x = lev.getStartX();
		y = lev.getStartY();
		lev.setStartX((matrices[trans][0][0]*x + matrices[trans][0][1]*y));
		lev.setStartY((matrices[trans][1][0]*x + matrices[trans][1][1]*y));
		
		for(BasicBlock b : r.blocks){
			if(b instanceof AnimatedBlock){
				for(Image i : ((AnimatedBlock) b).frames){
					rotateImage(i, trans);
				}
			}else{
				rotateImage(b.image, trans);
			}
		}
		rotateImage(r.button, trans);
	}
	
	private static void rotateImage(Image i, int trans){
		switch (trans){
			case 0: i.rotate(90); break;
			case 1: i.rotate(180); break;
			case 2: i.rotate(-90); break;
		}
	}
}
