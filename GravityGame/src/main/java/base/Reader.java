package base;

import java.io.*;

/**
 * This class handles all reading of sequential files.
 * 
 * @author Simon Rovder, Spojena skola Novohradska
 * @version 1.0 28 January 2013
 * 
 * HighwayGuru
 * IDE Eclipse 3.7.2
 * Platform: PC
 *
 */
public class Reader
{
	// The BufferedWriter - Variable used to read a sequential file.
    private BufferedReader reader;
    
    /**
     * Simple constructor - Create the instance of the class.
     */
    public Reader(){}
    
    /**
     * Opens a sequential file.
     * @param fileName The directory of the file that is to be opened.
     * @throws Exception
     */
    public void openFile(String fileName) throws Exception{
    	// Open the file.
	    this.reader = new BufferedReader(new FileReader(fileName));
    }
    
    /**
     * Reads the next line from the opened sequential file.
     * 
     * @return The read line.
     * @throws Exception
     */
    public String nextLine() throws Exception{
    	return this.reader.readLine();
    }
    
    /**
     * Closes the file.
     * @throws Exception
     */
    public void close() throws Exception{
    	this.reader.close(); // Close the file
    }
}