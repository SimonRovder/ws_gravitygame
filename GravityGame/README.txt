To launch the program, to the following:

In LINUX:
Run the LINUX_Launch.sh file (makes sure the program can find the relevant files).

In WINDOWS:
Run the WINDOWS_Launch.cmd file.

It is possible to run the GravityGame.jar file on it's own, but I recommend running the scripts.

Do not delete any files or folders in the Navigator main
directory (the one where GravityGame.jar is)!